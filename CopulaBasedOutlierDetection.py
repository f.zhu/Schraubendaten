from schrauben import Schraubendaten
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
from tsfresh.feature_extraction.settings import ComprehensiveFCParameters, EfficientFCParameters
from sklearn import preprocessing
sns.set()

if __name__ == '__main__':
    # feature generation
    tsfresh_extract_settings = {
        "mean": None,
        "standard_deviation": None,
        "skewness": None,
        "variance": None,
        "kurtosis": None,
        "median": None,
        # "abs_energy": None,
        "linear_trend": [{"attr": 'intercept'}],
        "mean_change": None,
        "variance_larger_than_standard_deviation": None,
    }  # tsfresh setting for feature generation
    #tsfresh_extract_settings = ComprehensiveFCParameters()
    to_extract_featurelist = ['/Channel/MachineAxis/aaLoad', '/Channel/MachineAxis/aaPower',
       '/Channel/MachineAxis/aaCurr', '/Channel/MachineAxis/aaPowerSmooth',
       '/Channel/MachineAxis/aaTorque', '/Channel/MachineAxis/aaVactB',
       '/Channel/MachineAxis/aaVactM', '/Channel/MachineAxis/actSpeedRel',
       '/Channel/MachineAxis/vaCurr', '/Channel/MachineAxis/vaLoad',
       '/Channel/MachineAxis/vaPower', '/Channel/MachineAxis/actFeedRate',
       '/Channel/Spindle/acSMode', '/Channel/Spindle/acSmaxAcc',
       '/Channel/Spindle/acSpindState', '/Channel/Spindle/actSpeed',
       '/Channel/Spindle/cmdAngPos', '/Channel/Spindle/driveLoad',
       '/Nck/MachineAxis/aaCurr', '/Nck/MachineAxis/aaLoad',
       '/Nck/MachineAxis/aaPower', '/Nck/MachineAxis/actToolBasePos',
       '/Nck/MachineAxis/driveImpulseEnabled', '/Nck/Spindle/actSpeed',
       '/Nck/Spindle/driveLoad','Schnittgeschwindigkeit', 'Zustelltiefe','Vorschub je Umdrehung', ]

    # feature selection
    method = 'VarianceThreshold'
    Schraubendata = Schraubendaten()

    with open("Data_180_with_id.pkl", "rb") as f:
        data = pickle.load(f)

    defektteil = [7, 32, 59, 71, 114, 120, 145, 159, 163, 169, 170, 172]
    defektteil = [x-1 for x in defektteil]
    Schraubendata.featuregeneration(data=data, to_extract_featurelist=to_extract_featurelist,
                                    tsfresh_extract_settings=tsfresh_extract_settings)
    Schraubendata.featureselection(method=method)
    x = Schraubendata.x_selected
    x = pd.DataFrame(data=x, index=x.index, columns=x.columns)


    # col = x_gesamt.columns.tolist()
    # for item in col:
    #     _, ax = plt.subplots(figsize=(10, 10))
    #     sns.displot(data=x_gesamt, x=item, hue='label')
    #     plt.title(item)
    #     name = item.replace('_', '-').replace('/','-').replace('"','-')
    #
    #     plt.savefig('test2/'+name+'.png')
    #     plt.close()

    # from sklearn.ensemble import IsolationForest
    #
    # clf = IsolationForest(random_state=0).fit(x)
    # y = clf.predict(x)
    # dd = pd.DataFrame(index=x.index, data=y)

    from pyod.models.copod import COPOD
    clf = COPOD().fit(x)
    y = clf.predict(x)
    score = clf.decision_scores_
    # clf.explain_outlier(144)

    dd = pd.DataFrame(index=x.index, data=y)
    dd.loc[:, 1] = None
    dd.iloc[:, 1][defektteil] = 'abnormal'
    dd.fillna(value='normal', inplace=True)
    dd.iloc[:, 0][dd.iloc[:, 0] == 1] = 'abnormal'
    dd.iloc[:, 0][dd.iloc[:, 0] == 0] = 'normal'

