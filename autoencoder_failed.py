import pandas as pd
import pickle
import keras
from keras.models import Sequential
from keras.layers import LSTM, Dense, Masking, RepeatVector,TimeDistributed, Dropout, Input
import tensorflow as tf
import numpy as np
import itertools
from keras.models import Model
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import os


with open("Data_180_with_id.pkl", "rb") as f:
    data = pickle.load(f)

data_load = data[['/Nck/MachineAxis/actToolBasePos', 'id']]


# train = [np.linspace(22,28,7).tolist(), np.linspace(72,77,6).tolist(), np.linspace(135,141,7).tolist()]
#train = [np.linspace(22,28,7).tolist()]
train = [[25]]
merged = list(itertools.chain(*train))

datatrain = data_load[data_load['id'].isin(merged)]
group = datatrain.groupby('id')
datalist = []
std = StandardScaler()
minmax = MinMaxScaler()
for item in group:
    x = item[1]['/Nck/MachineAxis/actToolBasePos'].to_numpy().reshape(-1, 1)
    #x = std.fit_transform(x)
    #x = x[800:900]
    #xx = np.linspace(0,1,200).reshape(-1,1)
    xx = minmax.fit_transform(x)
    datalist.append(xx)

max_len = max([x.__len__() for x in datalist])
for j, item in enumerate(datalist):
    datalist[j] = np.concatenate((item, np.array([0 for _ in range(max_len-len(item))]).reshape(-1, 1)), axis=0)
raw_inputs = np.array(datalist)
padded_inputs = raw_inputs.reshape(len(datalist), raw_inputs.shape[1], 1)


X_train = padded_inputs

checkpoint_path = "training_2/cp-{epoch:04d}.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path,
    verbose=1,
    save_weights_only=True,
    period=5)


visible = Input(shape=(X_train.shape[1], 1))
encoder = Masking(mask_value=0.0)(visible)
encoder = LSTM(200, activation='relu', return_sequences=True)(encoder)
encoder = LSTM(100, activation='relu', return_sequences=True)(encoder)
encoder = LSTM(100, activation='relu')(encoder)

decoder1 = RepeatVector(X_train.shape[1])(encoder)
decoder1 = LSTM(100, activation='relu', return_sequences=True)(decoder1)
decoder1 = LSTM(100, activation='relu', return_sequences=True)(decoder1)
decoder1 = LSTM(200, activation='relu', return_sequences=True)(decoder1)
decoder1 = TimeDistributed(Dense(1))(decoder1)

model = Model(inputs=visible, outputs=decoder1)
opt = keras.optimizers.Adam(learning_rate=0.001)
latest = tf.train.latest_checkpoint(checkpoint_dir)
model.load_weights(latest)
model.compile(optimizer=opt, loss='mse')
model.fit(X_train, X_train, epochs=300, verbose=1, callbacks=[cp_callback], initial_epoch=0)

pred = model.predict(X_train)
print(pred)