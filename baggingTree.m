clear
clc
data = readtable('data.csv');
load trainedmodel.mat

%% train modell with normal workspiece
%data1 = data(ismember(data.id, [1:4, 22:28, 72:77, 135:141]), :);
%regressionLearner

%% 
plot(data.x_Nck_MachineAxis_actToolBasePos)
hold on
yfit = trainedModel.predictFcn(data);
plot(yfit)
hold off

for i=1:max(data.id)
    yfit = trainedModel.predictFcn(data(find(data.id==i),:));
    mse(i) = mean((data(find(data.id==i),:).x_Nck_MachineAxis_actToolBasePos-yfit).^2);
end
mse(2,:) = linspace(1,180,180);

mse = mse';

Y = prctile(mse,90);

mse(mse(:,1)>=Y(1), 3) = 1;

defekt = [7,32,59,71,114,120,145,159,163,169,170,172];
result = cell(180,3);

ids = [1 0];
names = {'abnormal','normal'};
M = containers.Map(ids,names);

for i=1:size(mse,1)
    result{i,1} = mse(i,2);
    result{i,2} = M(mse(i,3));
    if ismember(i, defekt)
        result{i,3} = 'abnormal';
    else
        result{i,3} = 'normal';
    end
end

cm = confusionchart(result(:,3),result(:,2));
cm.Title = 'Abnormal Detection Using Bagging Tree';
cm.RowSummary = 'row-normalized';
cm.ColumnSummary = 'column-normalized';
