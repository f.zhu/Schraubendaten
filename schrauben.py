import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.base import BaseEstimator, TransformerMixin
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
from tsfresh import select_features
from tsfresh.feature_extraction import extract_features
from tsfresh.feature_extraction.settings import ComprehensiveFCParameters, EfficientFCParameters
from tsfresh.transformers.feature_selector import FeatureSelector
from sklearn.impute import KNNImputer, SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import VarianceThreshold
import re

sns.set()


def arrayzerlegen(array):
    b = []
    tast = False
    j = -1
    for i, item in enumerate(array):
        if i == array.__len__() - 1:
            break
        if item == 0 and array[i + 1] == 1:
            tast = True
            b.append([])
            j += 1
        elif item == 1 and array[i + 1] == 0:
            tast = False

        if tast:
            b[j].append(i + 1)
    return b


class Schraubendaten():
    def __init__(self, filename=''):
        self.datapath = 'data/' + filename
        self.labeled_prozess = 'data/labeled_prozess.csv'
        self.data = None
        self.x = None
        self.featurelist = None
        self.x_selected = None
        self.x_decomposed = None

    def data_arrange(self):
        df = pd.read_csv(self.datapath, index_col='Timestamp', parse_dates=True)
        prozess_list = pd.read_csv(self.labeled_prozess)
        prozesslist = arrayzerlegen(prozess_list.iloc[:, 0].tolist())
        todrop = ['Unnamed: 0', 'id', 'DTime']
        df.drop(columns=todrop, inplace=True)
        df.dropna(how='all', axis=1)
        __todrop_col__ = (df != df.iloc[0]).any()
        __todrop_col__ = __todrop_col__.index[__todrop_col__ == False]
        df.drop(columns=__todrop_col__, inplace=True)
        datalist = []
        for i, prozess in enumerate(prozesslist):
            df_prozess = df.iloc[prozess[0]: prozess[-1], :]
            df_prozess['id'] = i + 1
            datalist.append(df_prozess)
        self.data = pd.concat(datalist)

    def visualisationrowdata(self, to_visual):
        if self.data is None:
            raise ValueError('No data loaded!')
        group = self.data.groupby('id')
        for i, prozess in enumerate(group):
            fig = make_subplots(rows=len(to_visual), cols=1,
                                shared_xaxes=True,
                                vertical_spacing=0.02)
            for j, parameter in enumerate(to_visual):
                fig.add_trace(
                    go.Scatter(y=prozess[1][parameter], mode='lines', name=parameter), row=j + 1, col=1
                )
            fig.write_html(
                "./figure/werkstück_" + str(prozess[0]) + ".html")
            del fig

        fig = px.scatter_3d(self.data, x='Schnittgeschwindigkeit', y='Zustelltiefe', z='Vorschub je Umdrehung',
                            color='id')
        fig.write_html(
            "./figure/verteilung_prozess_config.html")

    def featuregeneration(self, to_extract_featurelist, tsfresh_extract_settings, n_neighbors=5, data=None):
        imputer = KNNImputer(n_neighbors=n_neighbors)

        class datatransform(BaseEstimator, TransformerMixin):
            def __init__(self, featurelist, settings, imputer):
                self.featurelist = featurelist
                self.settings = settings
                self.imputer = imputer

            def fit(self, X, y=None):
                return self

            def transform(self, X, y=None):
                _temp = X[X.columns[X.columns.str.contains('|'.join(self.featurelist + ['id']))]]
                _temp[:] = self.imputer.fit_transform(_temp)
                extracted_features = extract_features(_temp, default_fc_parameters=self.settings, column_id='id')
                extracted_features.sort_index(inplace=True)
                return extracted_features

        pipline_feature_generation = Pipeline([
            ('tranformer', datatransform(featurelist=to_extract_featurelist, settings=tsfresh_extract_settings,
                                         imputer=imputer))
        ])
        if data is None:
            x = pipline_feature_generation.fit_transform(self.data)
        else:
            x = pipline_feature_generation.fit_transform(data)
        x.dropna(inplace=True, axis=1)
        self.x = x

    def visualisation_generated_features(self):
        pass

    def featureselection(self, method):
        if method == 'VarianceThreshold':
            sel = VarianceThreshold(threshold=(.8 * (1 - .8)))
            x_selected = sel.fit_transform(self.x)

        self.x_selected = pd.DataFrame(columns=self.x.columns[sel.get_support(indices=True)], index=self.x.index, data=x_selected)

        log = pd.read_excel('data/VersucheLFF.xlsx', engine='openpyxl')
        log.dropna(how='any', inplace=True)

        pattern1 = re.compile(r'Teil.\d+.bis.\d+')
        pattern2 = re.compile(r'Teile.\d+-\d+|Teil.\d+-\d+')
        pattern3 = re.compile(r'Teil.\d+')

        df_log = pd.DataFrame(index=[i + 1 for i in range(180)],
                          columns=['Schnittgeschwindigkeit', 'Zustelltiefe', 'Vorschub je Umdrehung'])

        for i in range(log.__len__()):
            string = log.iloc[i]['Ereignis']
            if type(log.iloc[i]['Schnittgeschwindigkeit']) == str:
                s = float(re.findall('\d+', log.iloc[i]['Schnittgeschwindigkeit'])[0])
            else:
                s = log.iloc[i]['Schnittgeschwindigkeit']

            if type(log.iloc[i]['Zustelltiefe']) == str:
                z = float(re.findall('\d+', log.iloc[i]['Zustelltiefe'])[0])
            else:
                z = log.iloc[i]['Zustelltiefe']

            if type(log.iloc[i]['Vorschub je Umdrehung ']) == str:
                v = float(re.findall('\d+', log.iloc[i]['Vorschub je Umdrehung '])[0])
            else:
                v = log.iloc[i]['Vorschub je Umdrehung ']

            if pattern1.search(string):
                m = pattern1.search(string)
                num1 = int(m.group().split(' ')[1])
                num2 = int(m.group().split(' ')[-1])
            elif pattern2.search(string):
                m = pattern2.search(string).group()
                num1 = int(re.split(' |-', m)[1])
                num2 = int(re.split(' |-', m)[-1])
            elif pattern3.search(string):
                m = pattern3.search(string)
                num1 = int(m.group().split(' ')[1])
                num2 = num1
            else:
                num1 = np.nan
                num2 = np.nan

            df_log.iloc[num1 - 1:num2, :] = [s, z, v]

        df_log.fillna(method='ffill', inplace=True)
        self.x_selected = self.x_selected.join(df_log)

    def featuredecomposition(self):
        pass

    def demo(self):
        pass