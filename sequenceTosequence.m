clear
clc
dataraw = readtable('data.csv');
data = dataraw(ismember(dataraw.id, [22:28]), :);
id = unique(data.id);
for i = 1:length(id)
    datatest{i} = table2array(data(find(data.id==id(i)), {'x_Nck_MachineAxis_actToolBasePos'}))';
    sequenceLengths(i) = size(datatest{i},2);
end

[sequenceLengths,idx] = sort(sequenceLengths,'descend');
XTrain = datatest(idx)';
YTrain = XTrain;
% mu = mean([XTrain{:}],2);
% sig = std([XTrain{:}],0,2);
% 
% for i = 1:numel(XTrain)
%     XTrain{i} = (XTrain{i} - mu) ./ sig;
% end

miniBatchSize = 7;
numFeatures = size(XTrain{1},1);
numResponses = size(YTrain{1},1);
numHiddenUnits = 200;

layers = [ ...
    sequenceInputLayer(numFeatures)
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    lstmLayer(numHiddenUnits,'OutputMode','sequence')
    fullyConnectedLayer(50)
    dropoutLayer(0.2)
    fullyConnectedLayer(numResponses)
    regressionLayer];

maxEpochs = 400;
miniBatchSize = 20;

options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'InitialLearnRate',0.01, ...
    'GradientThreshold',1, ...
    'Shuffle','never', ...
    'Plots','training-progress',...
    'Verbose',0,...
    'ExecutionEnvironment','auto');

net = trainNetwork(XTrain,YTrain,layers,options);
save('net2.mat', 'net');


for i = 1:180
    test{i} = table2array(dataraw(find(dataraw.id==i), {'x_Nck_MachineAxis_actToolBasePos'}))';
end

YPredtest = predict(net,test,'MiniBatchSize',1,'ExecutionEnvironment','cpu');
for i=1:length(test)
    YPredtest{i,2} = test{i};
end

for i=1:180
    mse(i) = mean((YPredtest{i,1}-YPredtest{i,2}).^2);
end

mse(2,:) = linspace(1,180,180);
mse = mse';
Y = prctile(mse,90);

mse(mse(:,1)>=Y(1), 3) = 1;

defekt = [7,32,59,71,114,120,145,159,163,169,170,172];
result = cell(180,3);

ids = [1 0];
names = {'abnormal','normal'};
M = containers.Map(ids,names);

for i=1:size(mse,1)
    result{i,1} = mse(i,2);
    result{i,2} = M(mse(i,3));
    if ismember(i, defekt)
        result{i,3} = 'abnormal';
    else
        result{i,3} = 'normal';
    end
end

cm = confusionchart(result(:,3),result(:,2));
cm.Title = 'Abnormal Detection Using Sequence-to-Sequence Regression';
cm.RowSummary = 'row-normalized';
cm.ColumnSummary = 'column-normalized';