from schrauben import Schraubendaten
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()

if __name__ == '__main__':
    # data_arrange
    filename = 'DataRUB.csv'

    # visualisation
    to_visual = ['/Nck/MachineAxis/actToolBasePos', '/Channel/State/progStatus', '/Channel/MachineAxis/aaLoad']

    # feature generation
    tsfresh_extract_settings = {
        "mean": None,
        "standard_deviation": None,
        "maximum": None,
        "minimum": None,
        "skewness": None,
        "variance": None,
        "kurtosis": None,
        "median": None,
        "abs_energy": None,
        "linear_trend": [{"attr": 'intercept'}],
        "mean_change": None,
        "variance_larger_than_standard_deviation": None,
    }  # tsfresh setting for feature generation
    #tsfresh_extract_settings = ComprehensiveFCParameters()
    to_extract_featurelist = ['/Channel/MachineAxis/aaLoad', '/Nck/MachineAxis/actToolBasePos',
                              '/Channel/MachineAxis/aaPower'] #'Schnittgeschwindigkeit', 'Zustelltiefe', 'Vorschub je Umdrehung']

    # feature selection
    method = 'VarianceThreshold'

    Schraubendata = Schraubendaten(filename=filename)
    Schraubendata.data_arrange()
    # Schraubendata.visualisationrowdata(to_visual=to_visual)
    Schraubendata.featuregeneration(to_extract_featurelist=to_extract_featurelist,
                                    tsfresh_extract_settings=tsfresh_extract_settings)
    Schraubendata.featureselection(method=method)

    # group = Schraubendata.x_selected.groupby(['Schnittgeschwindigkeit', 'Zustelltiefe', 'Vorschub je Umdrehung'])
    # for item in group:
    #     pass
    #     for col in Schraubendata.x_selected.columns:
    #         _, ax = plt.subplots(figsize=(30, 10))
    #         ax.plot(item[1][col])
    #         ax.set_xticks(np.arange(min(item[1].index), max(item[1].index) + 1, 1.0))
    #         plt.title(col+'_'+str(item[0]))
    #         name = col.replace('/', '_')
    #         plt.savefig('test/'+name+'_'+str(item[0][0])+'.png')
    #         plt.close()

    # for col in Schraubendata.x_selected.columns:
    #     _, ax = plt.subplots(figsize=(30, 10))
    #     ax.plot(Schraubendata.x_selected[col])
    #     ax.set_xticks(np.arange(min(Schraubendata.x_selected.index), max(Schraubendata.x_selected.index) + 1, 2.0))
    #     plt.xticks(rotation=60)
    #     ax.set_title(col, fontsize=20)
    #     name = col.replace('/', '_')
    #     plt.savefig('test1/'+name+'.png')


    from sklearn.decomposition import PCA
    from minisom import MiniSom

    pca = PCA(n_components=3)
    newData = pca.fit_transform(Schraubendata.x_selected)
    som_shape = (4, 1)
    som = MiniSom(som_shape[0], som_shape[1], newData.shape[1], sigma=.35, learning_rate=.5,
                  neighborhood_function='gaussian', random_seed=10)

    som.pca_weights_init(newData)
    som.train_batch(newData, 100, verbose=True)

    winner_coordinates = np.array([som.winner(x) for x in newData]).T

    cluster_index = np.ravel_multi_index(winner_coordinates, som_shape)

    for c in np.unique(cluster_index):
        plt.scatter(newData[cluster_index == c, 0],
                    newData[cluster_index == c, 1], label='cluster=' + str(c), alpha=.7)
    # plotting centroids
    for centroid in som.get_weights():
        plt.scatter(centroid[:, 0], centroid[:, 1], marker='x',
                    s=80, linewidths=35, color='k', label='centroid')
        plt.xlabel('PCA1')
        plt.ylabel('PCA2')
    plt.title('Clustering with SOM for all 180 workpiece', fontsize=12)
    #plt.savefig('Clustering.png')
    plt.show()
    # import pickle
    #
    # with open('./Data_180_with_id.pkl', 'wb') as w:
    #     pickle.dump(x, w)
    # with open('./Data_180_with_id.pkl', 'rb') as r:
    #     data = pickle.load(r)
